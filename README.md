# DataCore Maven Artifacts Repository
APIs que n�o s�o encontradas no maven central.
>Mantenha atualizado!
```ps
mvn install:install-file -Dfile="suaApi.jar" -DlocalRepositoryPath="./repository" -DpomFile="seuPom.xml"
```

Adicione ao seu projeto:
```xml
	<repositories>
		<repository>
			<id>DataCoreRepo</id>
			<url>https://bitbucket.org/datacoresolutions/mavenartifacts/raw/master/repository/</url>
		</repository>
	</repositories>
```

Limpando hist�rico:
```ps
git checkout --orphan temp
git checkout master
git reset --hard temp
git push -f origin master
```